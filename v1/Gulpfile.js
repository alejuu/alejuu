var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence');

//Bulma
var input = './sass/*.sass';
var styles = './public/css/styles';
var stylesout = './public/css/styles/*.css';
var prefixer = './public/css/autoprefixer';
var prefixerout = './public/css/autoprefixer/*.css'
var min = './public';

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

//Bulma
gulp.task('sass', function () {
  return gulp
    // Find sass file from the `sass/` folder
    .src(input)
    // Run Sass on those files
    .pipe(sass(sassOptions).on('error', sass.logError))
    // Write the resulting CSS in the 'styles/' folder
    .pipe(gulp.dest(styles));
});

gulp.task('prefix', function () {
  return gulp
    // Find all `.scss` files from the `stylesheets/` folder
    .src(stylesout)
    // Run Autoprefixer on those files
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    // Write the resulting CSS in the autoprefixer folder
    .pipe(gulp.dest(prefixer));
});

gulp.task('minify', function() {
    return gulp
    .src(prefixerout)
    .pipe(cleanCSS({debug: true}, function(details) {
        console.log(details.name + ': ' + details.stats.originalSize);
        console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(gulp.dest(min));
});

// Create the default run action, which should be the entire build for Bulma
gulp.task('buildcss', function() {
	runSequence(
		'sass',
		'prefix',
		'minify'
	);
});
