<?php
/*
 Template Name: Work Mobile
 */
?>
<?php
get_header(); ?>

                                <?php
                                    $work_args = array(
                                      'category_name' => 'work'
                                    )
                                ?>
                                <?php $work = new WP_Query( $work_args ); ?>
                                <?php if ( $work->have_posts() ) : ?>
                                    <div class="content">
                                        <h1 class="title"><?php the_title(); ?></h1>
                                        <div class="columns">
                                            <?php while ( $work->have_posts() ) : $work->the_post(); ?>
                                                <div class="column is-one-third">
                                                    <div class="card">
                                                        <div class="card-image">
                                                            <a href="<?php echo esc_url( wp_get_shortlink()); ?>">
                                                                <?php $image_attributes = (is_singular() || in_the_loop()) ? wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ) : null; ?>
                                                                <figure class="image is-4by3 featured-img-bg" style="<?php if($image_attributes) echo 'background-image:url(\''.$image_attributes[0].'\')' ?>">
</figure>
                                                            </a>
                                                        </div>
                                                        <div class="card-content">
                                                            <div class="media">
                                                                <div class="media-content">
                                                                    <a href="<?php echo esc_url( wp_get_shortlink()); ?>"><p class="title is-4"><?php the_title(); ?></p></a>
                                                                    <!--p class="subtitle is-6" wp-get-the-date>Date</p-->
                                                                </div>
                                                            </div>
                                                            <div class="content">
                                                                <?php the_content(); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                            <?php wp_reset_postdata(); ?>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'alejuu' ); ?></p>
                                <?php endif; ?>

<?php get_footer(); ?>