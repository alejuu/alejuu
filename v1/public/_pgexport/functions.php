<?php
if ( ! function_exists( 'alejuu_setup' ) ) :

function alejuu_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     */
    /* Pinegrow generated Load Text Domain Begin */
    load_theme_textdomain( 'alejuu', get_template_directory() . '/languages' );
    /* Pinegrow generated Load Text Domain End */

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     */
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 825, 510, true );

    // Add menus.
    register_nav_menus( array(
        'primary' => __( 'Primary Menu', 'alejuu' ),
        'social'  => __( 'Social Links Menu', 'alejuu' ),
        'home'  => __( 'Home', 'alejuu' ),
        'work'  => __( 'Work Menu', 'alejuu' ),
        'me'  => __( 'Me Menu', 'alejuu' ),
        'connect'  => __( 'Connect Menu', 'alejuu' ),
        'mobile'  => __( 'Mobile Menu', 'alejuu' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
        'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
    ) );

    /*
     * Enable support for Post Formats.
     */
    add_theme_support( 'post-formats', array(
        'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
    ) );
}
endif; // alejuu_setup

add_action( 'after_setup_theme', 'alejuu_setup' );


if ( ! function_exists( 'alejuu_init' ) ) :

function alejuu_init() {


    // Use categories and tags with attachments
    register_taxonomy_for_object_type( 'category', 'attachment' );
    register_taxonomy_for_object_type( 'post_tag', 'attachment' );

    /*
     * Register custom post types. You can also move this code to a plugin.
     */
    /* Pinegrow generated Custom Post Types Begin */

    /* Pinegrow generated Custom Post Types End */

    /*
     * Register custom taxonomies. You can also move this code to a plugin.
     */
    /* Pinegrow generated Taxonomies Begin */

    /* Pinegrow generated Taxonomies End */

}
endif; // alejuu_setup

add_action( 'init', 'alejuu_init' );


if ( ! function_exists( 'alejuu_widgets_init' ) ) :

function alejuu_widgets_init() {

    /*
     * Register widget areas.
     */
    /* Pinegrow generated Register Sidebars Begin */

    /* Pinegrow generated Register Sidebars End */
}
add_action( 'widgets_init', 'alejuu_widgets_init' );
endif;// alejuu_widgets_init



if ( ! function_exists( 'alejuu_customize_register' ) ) :

function alejuu_customize_register( $wp_customize ) {
    // Do stuff with $wp_customize, the WP_Customize_Manager object.

    /* Pinegrow generated Customizer Controls Begin */

    /* Pinegrow generated Customizer Controls End */

}
add_action( 'customize_register', 'alejuu_customize_register' );
endif;// alejuu_customize_register


if ( ! function_exists( 'alejuu_enqueue_scripts' ) ) :
    function alejuu_enqueue_scripts() {

        /* Pinegrow generated Enqueue Scripts Begin */

    wp_deregister_script( 'scripts' );
    wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/scripts.js', false, null, true);

    /* Pinegrow generated Enqueue Scripts End */

        /* Pinegrow generated Enqueue Styles Begin */

    wp_deregister_style( 'styles' );
    wp_enqueue_style( 'styles', get_template_directory_uri() . '/styles.css?v=1.0', false, null, 'all');

    /* Pinegrow generated Enqueue Styles End */

    }
    add_action( 'wp_enqueue_scripts', 'alejuu_enqueue_scripts' );
endif;

/*
 * Resource files included by Pinegrow.
 */
/* Pinegrow generated Include Resources Begin */
require_once "inc/wp_smart_navwalker.php";

    /* Pinegrow generated Include Resources End */
?>
