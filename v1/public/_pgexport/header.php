<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="description" content="Front-end Web Developer">
        <meta name="author" content="alejuu">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body class="is-light">
        <div class="container is-fluid">
            <nav class="nav is-hidden-tablet-only is-hidden-desktop-only is-hidden-widescreen">
                <div class="nav-left">
                    <?php
                        PG_Smart_Walker_Nav_Menu::$options['template'] = '<a class="nav-item {CLASSES}" id="{ID}" {ATTRS}>{TITLE}</a>';
                        wp_nav_menu( array(
                          'menu' => 'mobile',
                          'container' => '',
                          'items_wrap' => '<div class="nav-left %2$s" id="%1$s">%3$s</div>',
                          'walker' => new PG_Smart_Walker_Nav_Menu()
                    ) ); ?>
                </div>
            </nav>
            <section class="is-light">
                <div class="hero-body">
                    <div class="container">
                        <div class="columns">
                            <div class="column is-one-quarter is-hidden-mobile">
                                <aside class="menu">
                                    <ul class="menu-list">
                                        <?php
                                            PG_Smart_Walker_Nav_Menu::$options['template'] = '<li id="{ID}" class="{CLASSES}">
                                                                  <a {ATTRS}>{TITLE}</a>
                                                                </li>';
                                            wp_nav_menu( array(
                                              'menu' => 'home',
                                              'container' => '',
                                              'items_wrap' => '<ul class="menu-list %2$s" id="%1$s">%3$s</ul>',
                                              'walker' => new PG_Smart_Walker_Nav_Menu()
                                        ) ); ?>
                                    </ul>
                                    <p class="menu-label"><?php _e( 'Work', 'alejuu' ); ?></p>
                                    <ul class="menu-list">
                                        <?php
                                            PG_Smart_Walker_Nav_Menu::$options['template'] = '<li id="{ID}" class="{CLASSES}">
                                                                  <a {ATTRS}>{TITLE}</a>
                                                                </li>';
                                            wp_nav_menu( array(
                                              'menu' => 'work',
                                              'container' => '',
                                              'items_wrap' => '<ul class="menu-list %2$s" id="%1$s">%3$s</ul>',
                                              'walker' => new PG_Smart_Walker_Nav_Menu()
                                        ) ); ?>
                                    </ul>
                                    <!-- <p class="menu-label">
                            Me</p>
                            <ul class="menu-list" wp-nav-menu="me" wp-nav-menu-type="smart" wp-nav-menu-set="content">
                              <li>
                                <a>Blog</a>
                              </li>
                            </ul> -->
                                    <p class="menu-label"><?php _e( 'Connect', 'alejuu' ); ?></p>
                                    <ul class="menu-list">
                                        <?php
                                            PG_Smart_Walker_Nav_Menu::$options['template'] = '<li id="{ID}" class="{CLASSES}">
                                                                  <a {ATTRS}>{TITLE}</a>
                                                                </li>';
                                            wp_nav_menu( array(
                                              'menu' => 'connect',
                                              'container' => '',
                                              'items_wrap' => '<ul class="menu-list %2$s" id="%1$s">%3$s</ul>',
                                              'walker' => new PG_Smart_Walker_Nav_Menu()
                                        ) ); ?>
                                    </ul>
                                </aside>
                            </div>
                            <div class="column">