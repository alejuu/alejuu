<?php
/*
 Template Name: Home
 */
?>
<?php
get_header(); ?>

<?php if ( have_posts() ) : ?>
    <div class="content">
        <?php while ( have_posts() ) : the_post(); ?>
            <div>
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </div>
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.', 'alejuu' ); ?></p>
<?php endif; ?>

<?php get_footer(); ?>