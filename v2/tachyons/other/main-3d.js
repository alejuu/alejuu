//canvas
var canvas = document.querySelector('#scene');
var width = canvas.offsetWidth,
    height = canvas.offsetHeight;

var renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
});
renderer.setPixelRatio(window.devicePixelRatio > 1 ? 2 : 1);
renderer.setSize(width, height);
renderer.setClearColor(0xA9E7DA);

var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera(100, width / height, 0.1, 10000);
camera.position.set(120, 0, 300);

var light = new THREE.HemisphereLight(0xffffff, 0x0C056D, 0.6);
scene.add(light);

var light = new THREE.DirectionalLight(0x590D82, 0.5);
light.position.set(200, 300, 400);
scene.add(light);
var light2 = light.clone();
light2.position.set(-200, 300, 400);
scene.add(light2);

var geometry = new THREE.IcosahedronGeometry(120, 4);
for(var i = 0; i < geometry.vertices.length; i++) {
    var vector = geometry.vertices[i];
    vector._o = vector.clone();
}
var material = new THREE.MeshPhongMaterial({
    emissive: 0x23f660,
    emissiveIntensity: 0.4,
    shininess: 0
});
var shape = new THREE.Mesh(geometry, material);
scene.add(shape);

function updateVertices (a) {
    for(var i = 0; i < geometry.vertices.length; i++) {
        var vector = geometry.vertices[i];
        vector.copy(vector._o);
        var perlin = noise.simplex3(
            (vector.x * 0.006) + (a * 0.0002),
            (vector.y * 0.006) + (a * 0.0003),
            (vector.z * 0.006)
        );
        var ratio = ((perlin*0.4 * (mouse.y+0.1)) + 0.8);
        vector.multiplyScalar(ratio);
    }
    geometry.verticesNeedUpdate = true;
}

function render(a) {
    requestAnimationFrame(render);
    updateVertices(a);
    renderer.render(scene, camera);
}

function onResize() {
    canvas.style.width = '';
    canvas.style.height = '';
    width = canvas.offsetWidth;
    height = canvas.offsetHeight;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
}

var mouse = new THREE.Vector2(0.8, 0.5);
function onMouseMove(e) {
    TweenMax.to(mouse, 0.8, {
        y: (e.clientY / height),
        x : (e.clientX / width),
        ease: Power1.easeOut
    });
}

requestAnimationFrame(render);
window.addEventListener("mousemove", onMouseMove);
var resizeTm;
window.addEventListener("resize", function(){
    resizeTm = clearTimeout(resizeTm);
    resizeTm = setTimeout(onResize, 200);
});
//Morphing
//anime
var polymorph1 = document.querySelector('#morphing .polymorph-1');
var polymorph2 = document.querySelector('#morphing .polymorph-2');
var polymorph3 = document.querySelector('#morphing .polymorph-3');
var polymorph4 = document.querySelector('#morphing .polymorph-4');

var cardheader1 = document.querySelector('.card-header-alejuu');
var cardheader2 = document.querySelector('.card-header-portfolio');
var cardheader3 = document.querySelector('.card-header-blog');
var cardheader4 = document.querySelector('.card-header-connect');

var card1 = document.querySelector('.card-alejuu');
var card2 = document.querySelector('.card-portfolio');
var card3 = document.querySelector('.card-blog');
var card4 = document.querySelector('.card-connect');

var vector1 = '827.86 329 1849.1 438.09 659.68 1147.2 129.37 506.27 827.86 54.75';
var vector2 = '830.61 555.68 1738.3 933.7 173.48 1108.6 173.48 313.1 956.38 32.36';
var vector3 = '848.22 538.8 1686.1 1053.3 356 1088.6 83.9 74.19 1156.6 158.53';
var vector4 = '1025 804.7 1732.7 1142.9 273.54 969.95 467.83 204.56 1259.6 239.38';
var vector5 = '1346.5 640.48 1736.3 1153.1 251.09 806.53 603.35 99.79 1591.7 175.81';
var vector6 = '1687.6 662.1 1566.9 1137.9 188.32 935.23 728.13 13.14 1682.4 281.19';
var vector7 = '1779.8 1135.7 1145.3 1011 149.74 624.2 1084.6 51.55 1779.8 569.93';
var vector8 = '1915 1197 834.03 1198 1258 330.01 1914.1 194.01';
var vector9 = '1447.6 1113 258.58 1096.6 267.37 192.88 1207.5 516.31 1792.3 641.81';
var vector10 = '1231.5 1092 209.8 942.13 431.13 65.9 1568.7 193.6 1537.5 827.27';
var vector11 = '880.79 1117.8 129.61 711.09 611.22 20.35 1653.4 419.79 1213.7 796.7';
var vector12 = '698.63 1103.3 296.05 511.85 821.6 84.87 1640.8 659.94 1578.5 1126.8';
var vector13 = '698.630 1103.290, 296.050 511.850, 821.600 84.870, 1640.760 659.940, 1578.460 1126.800, 698.630 1103.290';
var vector14 = '89.64 786.98 538.35 67 1215.5 69.21 1903 776.98 1053.3 994.45';
var vector15 = '349.71 494.1 730.08 61.75 1364.2 140.8 1794.2 1028.9 730.08 1048.1';
var vector16 = '427.97 281.77 900.9 37.21 1500.5 222.22 1820.7 940.48 434.63 1063.2';

function animateButton1(loop) {
  anime.remove(polymorph2);
	anime.remove(polymorph3);
	anime.remove(polymorph4);
  anime.remove(polymorph1);
  anime({
    targets: polymorph1,
    points: [
      { value: vector1 },
      { value: vector2 },
      { value: vector3 },
      { value: vector4 }
    ],
    fill: [
      {value: 'rgba(62,83,104,.8)'},
      {value: 'rgba(59,62,69,.4)'},
      {value: 'rgba(162,144,99,.7)'},
      {value: 'rgba(131,157,162,.5)'}
    ],
    opacity: [.3,1,0],
    easing: 'easeOutQuad',
    duration: 3000,
    loop: loop
  });
}

function animateButton2(loop) {
  anime.remove(polymorph1);
	anime.remove(polymorph3);
	anime.remove(polymorph4);
  anime.remove(polymorph2);
  anime({
    targets: polymorph2,
    points: [
      { value: vector5 },
      { value: vector6 },
      { value: vector7 },
      { value: vector8 }
    ],
    fill: [
      {value: 'rgba(131,157,162,.8)'},
      {value: 'rgba(117,118,118,.6)'},
      {value: 'rgba(62,83,104,.5)'},
      {value: 'rgba(59,62,69,.9)'}
    ],
    opacity: [.3,1,0],
    easing: 'easeOutQuad',
    duration: 3000,
    loop: loop
  });
}

function animateButton3(loop) {
  anime.remove(polymorph1);
  anime.remove(polymorph2);
	anime.remove(polymorph4);
	anime.remove(polymorph3);
  anime({
    targets: polymorph3,
    points: [
      { value: vector9 },
      { value: vector10 },
      { value: vector11 },
      { value: vector12 }
    ],
    fill: [
			{value: 'rgba(117,118,118,.7)'},
      {value: 'rgba(59,62,69,.7)'},
			{value: 'rgba(62,83,104,.4)'},
      {value: 'rgba(131,157,162,.8)'}
    ],
    opacity: [.3,1,0],
    easing: 'easeOutQuad',
    duration: 3000,
    loop: loop
  });
}

function animateButton4(loop) {
  anime.remove(polymorph1);
  anime.remove(polymorph2);
	anime.remove(polymorph3);
	anime.remove(polymorph4);
  anime({
    targets: polymorph4,
    points: [
      { value: vector9 },
      { value: vector10 },
      { value: vector11 },
      { value: vector12 }
    ],
    fill: [
			{value: 'rgba(162,144,99,.9)'},
      {value: 'rgba(59,62,69,.4)'},
			{value: 'rgba(131,157,162,.6)'},
      {value: 'rgba(62,83,104,.7)'}
    ],
    opacity: [.3,1,0],
    easing: 'easeOutQuad',
    duration: 3000,
    loop: loop
  });
}

function animateHeader1(loop) {
	anime.remove(cardheader1);
	anime({
		targets: cardheader1,
		easing: 'easeOutQuad',
    duration: 3000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateHeader2(loop) {
	anime.remove(cardheader2);
	anime({
		targets: cardheader2,
		easing: 'easeOutQuad',
    duration: 3000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateHeader3(loop) {
	anime.remove(cardheader3);
	anime({
		targets: cardheader3,
		easing: 'easeOutQuad',
    duration: 3000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateHeader4(loop) {
	anime.remove(cardheader4);
	anime.remove(card4);
	anime({
		targets: cardheader4,
		easing: 'easeOutQuad',
    duration: 3000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateCard1(loop) {
	anime.remove(card1);
	anime({
		targets: card1,
		easing: 'easeOutQuad',
    duration: 1000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateCard2(loop) {
	anime.remove(card2);
	anime({
		targets: card2,
		easing: 'easeOutQuad',
    duration: 1000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateCard3(loop) {
	anime.remove(card3);
	anime({
		targets: card3,
		easing: 'easeOutQuad',
    duration: 1000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

function animateCard4(loop) {
	anime.remove(card4);
	anime({
		targets: card4,
		easing: 'easeOutQuad',
    duration: 1000,
		delay: 1000,
		opacity: [0,1],
		loop: 1
	});
}

//scrollify
$(function() {
	$.scrollify({
		section : '.snap',
		sectionName : 'section-name',
		easing: 'swing',
		scrollSpeed: 1100,
		// scrollbars: false,
		before: function(i,panels) {
      var ref = panels[i].attr("data-section-name");
			if(ref==="about"){
        animateButton1('true');
				animateHeader1('true');
				animateCard1('true');
      }
			if(ref==="portfolio"){
        animateButton2('true');
				animateHeader2('true');
				animateCard2('true');
      }
			if(ref==="blog"){
        animateButton3('true');
				animateHeader3('true');
				animateCard3('true');
      }
			if(ref==="connect"){
        animateButton4('true');
				animateHeader4('true');
				animateCard4('true');
      }
    },
    after: function() {},
    afterResize: function() {},
		afterRender:function() {
      $(".pagination a").on("click",$.scrollify.move);
    }
	});
});

jQuery(document).ready(function(){
	//Nav
	if( $('.cd-stretchy-nav').length > 0 ) {
		var stretchyNavs = $('.cd-stretchy-nav');

		stretchyNavs.each(function(){
			var stretchyNav = $(this),
			stretchyNavTrigger = stretchyNav.find('.cd-nav-trigger');

			stretchyNavTrigger.on('click', function(event){
				event.preventDefault();
				stretchyNav.toggleClass('nav-is-visible');
			});
		});

		$(document).on('click', function(event){
			( !$(event.target).is('.cd-nav-trigger') && !$(event.target).is('.cd-nav-trigger span') ) && stretchyNavs.removeClass('nav-is-visible');
		});
	};
	//slick
	$('#carousel-work, #carousel-blog').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		arrows: true
	});
  //animejs
  animateButton1('true');
  animateHeader1('true');
  animateCard1('true');
});
